module.exports = function(grunt){
	grunt.initConfig({
		jshint: {
			all:['script1.js']
		},
		concat: { 
			dist:{
				src: ['script1.js','script2.js','script3.js'],
				dest: 'scriptAll.js'
			}
		},
		uglify: {
			dist:{
				src: 'scriptAll.js',
				dest: 'build/scriptAll.min.js'
			}
		},
		shell: {
			multiple: {
				command: [
					'del scriptAll.js',
					'mkdir deploy',
					'move build\\scriptAll.min.js deploy\\scriptAll.min.js'
				].join('&&')
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-shell');

	grunt.registerTask('default', ['jshint','concat','uglify','shell']);
}; 